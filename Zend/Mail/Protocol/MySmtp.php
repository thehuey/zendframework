<?php

/**
 * Zend Framework
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://framework.zend.com/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@zend.com so we can send you a copy immediately.
 * 
 * @category   Zend
 * @package    Zend_Mail
 * @subpackage Protocol
 * @copyright  Copyright (c) 2005-2008 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 * @version    $Id: Smtp.php 8064 2008-02-16 10:58:39Z thomas $
 */


/**
 * @see Zend_Mime
 */
require_once 'Zend/Mime.php';


/**
 * @see Zend_Mail_Protocol_Abstract
 */
require_once 'Zend/Mail/Protocol/Abstract.php';


/**
 * Smtp implementation of Zend_Mail_Protocol_Abstract
 *
 * Minimum implementation according to RFC2821: EHLO, MAIL FROM, RCPT TO, DATA, RSET, NOOP, QUIT
 * 
 * @category   Zend
 * @package    Zend_Mail
 * @subpackage Protocol
 * @copyright  Copyright (c) 2005-2008 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */
class Zend_Mail_Protocol_MySmtp extends Zend_Mail_Protocol_Smtp 
{
    /**
     * Connect to the server with the parameters given in the constructor.
     *
     * @param array $options, Array of options to allow for special stream handling
     * @return boolean
     */
    public function connect(Array $options = array())
    {
        if (empty($options)) {
            return $this->_connect($this->_transport . '://' . $this->_host . ':'. $this->_port);
        } else {
          $remote = $this->_transport . '://' . $this->_host . ':'. $this->_port;
          $errorNum = 0;
          $errorStr = '';
  
          // open connection
          $context = stream_context_create($options);
          $this->_socket = stream_socket_client($remote, $errorNum, $errorStr, self::TIMEOUT_CONNECTION, STREAM_CLIENT_CONNECT, $context);
  
          if ($this->_socket === false) {
              if ($errorNum == 0) {
                  $errorStr = 'Could not open socket';
              }
              /**
               * @see Zend_Mail_Protocol_Exception
               */
              require_once 'Zend/Mail/Protocol/Exception.php';
              throw new Zend_Mail_Protocol_Exception($errorStr);
          }
  
          if (($result = stream_set_timeout($this->_socket, self::TIMEOUT_CONNECTION)) === false) {
              /**
               * @see Zend_Mail_Protocol_Exception
               */
              require_once 'Zend/Mail/Protocol/Exception.php';
              throw new Zend_Mail_Protocol_Exception('Could not set stream timeout');
          }
  
          return $result;
        }
    }
    public function __construct($host = '127.0.0.1', $port = null, $config = null)
    {
        if (is_array($config)) {
            if (isset($config['username'])) {
                $this->_username = $config['username'];
            }
            if (isset($config['password'])) {
                $this->_password = $config['password'];
            }
        }

        parent::__construct($host, $port, $config);
    }


    /**
     * Perform LOGIN authentication with supplied credentials
     *
     * @return void
     */
    public function auth()
    {
        // Ensure AUTH has not already been initiated.
        parent::auth();

        $this->_send('AUTH LOGIN');
        $this->_expect(334);
        $this->_send(base64_encode($this->_username));
        $this->_expect(334);
        $this->_send(base64_encode($this->_password));
        $this->_expect(235);
        $this->_auth = true;
    }
}
