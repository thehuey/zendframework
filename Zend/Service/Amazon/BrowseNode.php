<?php

/**
 * Zend Framework
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://framework.zend.com/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@zend.com so we can send you a copy immediately.
 *
 * @category   Zend
 * @package    Zend_Service
 * @subpackage Amazon
 * @copyright  Copyright (c) 2005-2009 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 * @version    $Id: Item.php 16211 2009-06-21 19:23:55Z thomas $
 */


/**
 * @category   Zend
 * @package    Zend_Service
 * @subpackage Amazon
 * @copyright  Copyright (c) 2005-2009 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */
class Zend_Service_Amazon_BrowseNode
{
    /**
     * @var int
     */
    public $BrowseNodeId;

    /**
     * @var string
     */
    public $Name;

    /**
     * @var Zend_Service_Amazon_BrowseNode[]
     */
    public $Ancestors = array();

    /**
     * @var int
     */
    public $TopSellers = array();

    /**
     * @var int
     */
    public $TopItemSet = array();

    protected $_dom;


    /**
     * Parse the given <Item> element
     *
     * @param  DOMElement $dom
     * @return void
     */
    public function __construct(DOMElement $dom)
    {
        $xpath = new DOMXPath($dom->ownerDocument);
        $xpath->registerNamespace('az', $dom->ownerDocument->documentElement->getAttribute('xmlns'));
        $this->BrowseNodeId = $xpath->query('./az:BrowseNodeId/text()', $dom)->item(0)->data;
        $this->Name = $xpath->query('./az:Name/text()', $dom)->item(0)->data;

		$ancestors = $xpath->query('./az:Ancestors/az:BrowseNode', $dom);
		#print "Length: {$ancestors->length}\n";
		for ($i = 0;$i<$ancestors->length; $i++) {
			$ancestor = new Zend_Service_Amazon_BrowseNode( $ancestors->item($i) );
			$this->Ancestors[] = $ancestor;
		}

        $topsellers = $xpath->query('./az:TopSellers/az:TopSeller', $dom);
        for ($i=0; $i < $topsellers->length; $i++) {
            $this->TopSellers[] = array(
				'ASIN' => $xpath->query('./az:ASIN/text()', $topsellers->item($i))->item(0)->data,
				'Title'	=> $xpath->query('./az:Title/text()', $topsellers->item($i))->item(0)->data
			);
        }

        $topitemset = $xpath->query('./az:TopItemSet/az:TopItem', $dom);
        for ($i=0; $i < $topitemset->length; $i++) {
            $item = array(
				'ASIN' => $xpath->query('./az:ASIN/text()', $topitemset->item($i))->item(0)->data,
				'Title'	=> $xpath->query('./az:Title/text()', $topitemset->item($i))->item(0)->data,
				'DetailPageURL'	=> $xpath->query('./az:DetailPageURL/text()', $topitemset->item($i))->item(0)->data,
				'ProductGroup'	=> $xpath->query('./az:ProductGroup/text()', $topitemset->item($i))->item(0)->data,
			);
			$authors = $xpath->query('./az:Actor', $topitemset->item($i));
			foreach ($authors as $author) {
				$item['Author'][] = $author->nodeValue;
			}
			$this->TopItemSet[] = $item;
        }
        $this->_dom = $dom;
    }


    /**
     * Returns the item's original XML
     *
     * @return string
     */
    public function asXml()
    {
        return $this->_dom->ownerDocument->saveXML($this->_dom);
    }
}
