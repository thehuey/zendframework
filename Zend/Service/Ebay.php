<?php
require_once 'Zend/Cache.php';

class Zend_Service_Ebay {
  public function __construct($trading = false)
  {
    $this->isTrading = $trading;
    if (!$trading) {
        require_once 'apis/ebay/shop/EbatNs_ServiceProxyShopping.php';
        require_once 'apis/ebay/shop/EbatNs_Logger.php';
    } else {
        require_once 'apis/ebay/trading/EbatNs_ServiceProxy.php';
        require_once 'apis/ebay/trading/EbatNs_Logger.php';
    }
    $this->session = new EbatNs_Session();
    $this->session->setAppId('HueyHu56e-62db-454d-b5a9-0984fe11030');
    $this->session->setSiteId(0);

    if (!$trading) {
      $this->cs = new EbatNs_ServiceProxyShopping($this->session);
    } else {
      $this->cs = new EbatNs_ServiceProxy($this->session);
    }
  }
  
  public function doSearch($options) 
  {
    require_once('apis/ebay/shop/FindItemsAdvancedRequestType.php');
    require_once('apis/ebay/shop/FindItemsAdvancedResponseType.php');
    require_once('apis/ebay/shop/CategoryType.php');
    $req = new FindItemsAdvancedRequestType();
    $query = filter_var($options['query']);
    $req->setQueryKeywords($query);
    if (!isset($options['perpage'])) $req->setMaxEntries(10);
    else $req->setMaxEntries($options['perpage']);
    if (isset($options['page'])) $req->setPageNumber($options['page']);

    if (isset($options['sortby']) && isset($options['sortorder'])) 
    {
        ## Valid Values are: BestMatch CustomCode EndTime BidCount Country
        ## CurrentBid Distance StartDate BestMatchCategoryGroup
        $req->setItemSort($options['sortby']);
        ## Valid Values are: Ascending Descending CustomCode
        $req->setSortOrder($options['sortorder']);
    }

    if (isset($options['catid'])) 
    {
        $req->setCategoryID($options['catid']);
    }
#$CountryCode = isset($_REQUEST['cc']) ? $_REQUEST['cc'] : $Facet_CountryCodeType->US;
#$PostalCode  = isset($_REQUEST['zip']) ? $_REQUEST['zip'] : '95051';

    #$req->setPostalCode($PostalCode);

    ## Include Selector Values: Details, SearchDetails(*), SellerInfo, ItemSpecifics,
    ##          ExpansionItemCount, CategoryHistogram
    $req->setIncludeSelector('Details,SellerInfo');

    $frontoptions = array(
        'caching' => true,
        'lifetime' => 120,
        'automatic_serialization' => true,
        'ignore_user_abort' => true
    );
    $backoptions = array(
        'cache_dir' => '/home/apexmotorsports.com/cache',
        'hashed_directory_level' => 2,
        #'hashed_directory_umask' => '0700'
    );
    $cache = Zend_Cache::factory('Core', 'File', $frontoptions, $backoptions);
    if ($cache) {
      ksort($options);
      $cache_key = md5(join('_', $options));
#print $cache_key;
      if (!($res = $cache->load($cache_key))) {
        $res = $this->cs->FindItemsAdvanced($req);
        $cache->save($res, $cache_key);
      }
    } else {
      $res = $this->cs->FindItemsAdvanced($req);
    }
# print_r($res);
    #$items = $res->getSearchResult();
    return $res;
  }
  public function getItem($item)
  {
    require_once 'apis/ebay/shop/GetSingleItemRequestType.php';
    $req = new GetSingleItemRequestType();
    $req->setItemID($item);
    $req->setIncludeSelector('Description,ItemSpecifics');
    $res = $this->cs->GetSingleItem($req);
    return $res->Item;
  }
  public function __call($name, $argv)
  {
    $argc = count($argv);
    if (substr($name, 0, 4) == 'find' || 
        $name == 'getMultipleItems' ||
        $name == 'getSingleItem'    )
    {
      require_once 'apis/ebay/shop/' . ucfirst($name) . 'RequestType.php';
      require_once 'apis/ebay/shop/' . ucfirst($name) . 'ResponseType.php';
    } else if (substr($name, 0, 3) == 'get')
    {
      require_once 'apis/ebay/trading/' . ucfirst($name) . 'RequestType.php';
      require_once 'apis/ebay/trading/' . ucfirst($name) . 'ResponseType.php';
    }
    if (1)
    {
        
      if (is_array($argv[0]) && $argv[0]['debug'])
      {
        $this->debug = 1; $this->cs->attachLogger(new EbatNs_Logger()); unset($argv[0]['debug']);
      }
      $reqName = ucfirst($name) . 'RequestType';
      $req = new $reqName();
      if (is_array($argv[0]))
      {
        foreach ($argv[0] as $key => $val)
        {
          if (is_callable(array($req, 'set'.ucfirst($key))))
          {
            $func = 'set'.ucfirst($key);
            $req->$func($val);
          }
        }
      }

      if ($this->isTrading) {
        return $this->cs->call(ucfirst($name), $req);
      } else {
        return $this->cs->callShoppingApiStyle(ucfirst($name), $req);
#print_r($req);
      }
    }
  }
}

class EbaySearch {
  public function __construct() {
    require_once('Zend/Rest/Client.php');
    $this->_rest = new Zend_Rest_Client('http://lapi.ebay.com');
    $this->_defaults = array(
		'ai' => 'hcso',
		'bin' => 'n',
		'cid' => '0',
        #'catid' => '',
		'eksize' => '1',
		'encode' => 'ISO-8859-1',
		#'endcolor' => 'FF0000',
		'endtime' => 'y',
		#'fbgcolor' => 'EFEFEF',
		#'fntcolor' => '000000',
		'fs' => '0',
		#'hdrcolor' => 'FFFFCC',
		'hdrimage' => '9',
		'hdrsrch' => 'y',
		'img' => 'n',
		#'lnkcolor' => '0000FF',
		'logo' => '11',
		'num' => '20',
		'numbid' => 'y',
		'paypal' => 'y',
		'popup' => 'y',
		'prvd' => '1',
		'height' => '400',
        'query' => '',
		'r0' => '4',
		'shipcost' => 'y',
		'siteid' => '0',
		'sort' => 'MetaEndSort',
		'sortby' => 'endtime',
		'sortdir' => 'asc',
		'srchdesc' => 'n',
		#'tbgcolor' => 'FFFFFF',
		#'tlecolor' => '666600',
		'tlefs' => '0',
		#'tlfcolor' => 'FFFFFF',
		'track' => '2418643',
		'width' => '400',
		'xml' => 'y');
  }

  public function setQuery($query) {
    $this->_defaults['query'] = urlencode($query);
  }

  public function doSearch($options) {
    $client = new Zend_Http_Client('http://lapi.ebay.com');
    $options = array_merge($this->_defaults, $options);

    foreach ($options as $key => $option) {
        $uri[] = "$key=$option";
    }
    $uri_path = join('&', $uri);
    #$uri_path = preg_replace('#\$query#', $this->query, $uri_path);
    $uri_path = 'http://lapi.ebay.com/ws/eBayISAPI.dll?EKServer&' . $uri_path;
    $client->setUri($uri_path);
    $this->_resp = $client->request();
    $this->_doc = new DOMDocument();

    $this->_doc->loadXML($this->_resp->getBody());
    $results = new eBayResultSet($this->_doc);
    return $results;
  }

  public function setDocument($doc) {
    $this->_doc = new DOMDocument();
    $this->_doc->load($doc);
    $results = new eBayResultSet($this->_doc);
    return $results;
  }
}

class eBayResultSet implements SeekableIterator {

  protected $_currentIndex = 0;

  public function __construct(DOMDocument $dom) {
    $xp = new DOMXPath($dom);

    $this->Count = $xp->query("./Search/Count/text()")->item(0)->data;
    $this->GrandTotal = $xp->query("./Search/GrandTotal/text()")->item(0)->data;

    $this->_results = $xp->query("//Item");
  }
    /**
     * Implement SeekableIterator::current()
     *
     * Must be implemented by child classes
     *
     * @throws Zend_Service_Exception
     * @return Zend_Service_Yahoo_Result
     */
    public function current()
    {
        return new eBayResult($this->_results->item($this->_currentIndex));
    }


    /**
     * Implement SeekableIterator::key()
     *
     * @return int
     */
    public function key()
    {
        return $this->_currentIndex;
    }


    /**
     * Implement SeekableIterator::next()
     *
     * @return void
     */
    public function next()
    {
        $this->_currentIndex += 1;
    }


    /**
     * Implement SeekableIterator::rewind()
     *
     * @return void
     */
    public function rewind()
    {
        $this->_currentIndex = 0;
    }


    /**
     * Implement SeekableIterator::seek()
     *
     * @param  int $index
     * @throws Zend_Service_Exception
     * @return Zend_Service_Yahoo_Result
     */
    public function seek($index)
    {
        $indexInt = (int) $index;
        if ($indexInt >= 0 && $indexInt < $this->_results->length) {
            $this->_currentIndex = $indexInt;
        } else {
            throw new OutOfBoundsException("Illegal index '$index'");
        }
    }

    public function item($index, $seek = false)
    {
        $oldIdx = $this->_currentIndex;
        $this->seek($index);
        $item = $this->current();
        if (!$seek) $this->seek($oldIdx);
        return $item;
    }
    /**
     * Implement SeekableIterator::valid()
     *
     * @return boolean
     */
    public function valid()
    {
        return $this->_currentIndex < $this->_results->length;
    }
}

class eBayResult {
    public function __construct(DOMElement $result) {
        // default fields for all search results:
        $fields = array('Title', 'Id', 'SubtitleText', 'CurrencyId', 'Country', 'Link',
                'CurrentPrice', 'LocalizedCurrentPrice', 'BINPrice', 'BidCount',
                'StartTime', 'EndTime', 'ItemProperties/Gallery', 'ItemProperties/GalleryURL',
                'ShippingDetail/DefaultShippingCost');

        // merge w/ child's fields
        #$this->_fields = array_merge($this->_fields, $fields);
        $this->_fields = $fields;

        $this->_xpath = new DOMXPath($result->ownerDocument);
        #$this->_xpath->registerNamespace('yh', $this->_namespace);

        // add search results to appropriate fields

        foreach ($this->_fields as $f) {
            #$query = "./yh:$f/text()";
            $query = "./$f/text()";
            $node = $this->_xpath->query($query, $result);
            $tmp_f = str_replace("/", "_", $f);
            if ($node->length == 1) {
                $this->{$tmp_f} = $node->item(0)->data;
            } else {
                $this->{$tmp_f} = null;
            }
        }

        $this->_result = $result;
    }
}
